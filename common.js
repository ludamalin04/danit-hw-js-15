'use strict'
let factorialCount = function (){
    let num = +prompt('Enter number');
    let result = 1;
    for (let i=1; i<=num; i++){
        result *= i;
    }
    alert(`Factorial number for ${num} is ${result}`);
}
factorialCount();